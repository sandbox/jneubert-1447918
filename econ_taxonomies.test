<?php

/**
 * Test looking up and storing web taxonomy terms.
 */
class EconTaxonomiesTestCase extends DrupalWebTestCase {

  public static function getInfo() {
    return array(
      'name' => 'Lookup and storing web taxonomy terms',
      'description' => 'Verify creating example nodes, accessing terms and attatching them to example nodes.',
      'group' => 'Web Taxonomy',
    );
  }

  function setUp() {
    parent::setUp('econ_taxonomies_example', 'devel');

    // Set default storage backend.
    variable_set('field_storage_default', 'field_sql_storage');

    $web_user = $this->drupalCreateUser(array('create econ_taxonomies_example content', 'use web_taxonomy'));
    $this->drupalLogin($web_user);
  }

  // The datasets for the tests.
  protected $datasets = array(
    'stw' => array(
      'url' => 'http://zbw.eu/beta/econ-ws/suggest',
      'example_terms' => array(
        array(
          'input' => 'theor',
          'term_name' => 'Theory',
          'uri' => 'http://zbw.eu/stw/descriptor/19073-6',
        ),
        array(
          'input' => 'bern',
          'term_name' => 'Berne (Canton)',
          'uri' => 'http://zbw.eu/stw/descriptor/16959-0',
        ),
        /** does not work due to sparql parsing error
        array(
          'input' => '"berne (can"',
          'term_name' => 'Berne (Canton)',
          'uri' => 'http://zbw.eu/stw/descriptor/16959-0',
        ),
        **/
      ),
    ),
    'econ_pers' => array(
      'url' => 'http://zbw.eu/beta/econ-ws/suggest2',
      'example_terms' => array(
        array(
          'input' => 'marx',
          'term_name' => 'Marx, Karl (1818 - 1883; Philosoph, Politiker)',
          'uri' => 'http://d-nb.info/gnd/118578537',
        ),
        array(
          'input' => '"marx, k"',
          'term_name' => 'Marx, Karl (1818 - 1883; Philosoph, Politiker)',
          'uri' => 'http://d-nb.info/gnd/118578537',
        ),
      ),
    ),
    'econ_corp' => array(
      'url' => 'http://zbw.eu/beta/econ-ws/suggest2',
    ),
  );

  /**
   * Create a "Example economics publication" node (w/o taxonomy terms) and ensure it serialized properly.
   */
  function testEconTaxonomiesExampleNodeCreation() {
    // Create a node.
    $edit = array();
    $langcode = LANGUAGE_NONE;
    $edit["title"] = $this->randomName(8);
    $edit["body[$langcode][0][value]"] = $this->randomName(16);
    $this->drupalPost('node/add/econ-taxonomies-example', $edit, t('Save'));

    // Check that the Example economics publication has been created.
    $this->assertRaw(t('!post %title has been created.', array('!post' => 'Example economics publication', '%title' => $edit["title"])), t('Example economics publication created.'));

    // Check that the node exists in the database.
    $node = $this->drupalGetNodeByTitle($edit["title"]);
    $this->assertTrue($node, t('Node found in database.'));
  }

  /**
   * Verify that the web services are available in itself
   */
  function testEconTaxonomiesExampleWebServicesAvailibility() {

    // Check each service/dataset by http request
    $http = http_client();
    foreach ($this->datasets as $dataset_name => $dataset) {
      $params = array(
        'dataset' => $dataset_name,
        'query' => $this->randomName(8),
        'output' => 'sparql-json',
      );
      $data = $http->get($dataset['url'], $params);

      // Check that the data has the expected structure
      $json_result = json_decode($data);
      $this->assertTrue(isset($json_result->results->bindings), t('Web service for @dataset available', array('@dataset' => $dataset_name)));
    }
  }

  /**
   * Test autocomplete function.
   **/
  function testAutocomplete() {

    foreach ($this->datasets as $dataset_name => $dataset) {
      if ($dataset_name == 'econ_corp') {
        continue;
      }
      foreach ($dataset['example_terms'] as $term) {
        $response = $this->drupalGet("web_taxonomy/autocomplete/$dataset_name/" . $term['input']);
        // The term will be quoted, and the " will be encoded in unicode (\u0022).
        // PCRE does not support \u
        $search_with_quotes = strpos($response, '"\u0022' . $term['term_name'] . '\u0022":"' . $term['term_name'] . '"');
        $search_without_quotes = strpos($response, '"' . $term['term_name'] . '":"' . $term['term_name'] . '"');
        $this->assertTrue($search_with_quotes or $search_without_quotes, 
            format_string('Autocomplete returns term %term_name after typing %input.', array('%term_name' =>  $term['term_name'], '%input' => $term['input'])));
      }
    }
  }

/**
DOES NOT WORK - lookup and select functionality missing
  /**
   * Test term creation with a web vocabulary from the node form.
   /

  function testNodeTermCreationAndDeletion() {
    $terms = array(
      'term1' => array(
        'input' => 'theory',
        'literal' => 'Theory',
        'uri' => 'http://zbw.eu/stw/descriptor/19073-6',
      ),
    );

    $edit = array();
    $langcode = LANGUAGE_NONE;
    $edit["title"] = $this->randomName();
    $edit["body[$langcode][0][value]"] = $this->randomName();
    $edit['et_ex_subject'][$langcode] = drupal_implode_tags(array($terms['term1']['input']));
    debug($edit);

    // Save, creating the terms.
    $this->drupalPost('node/add/econ-taxonomies-example', $edit, t('Save'));
    $this->assertRaw(t('@type %title has been created.', array('@type' => t('Example economics publication'), '%title' => $edit["title"])), 'The node was created successfully.');
    foreach ($terms as $term) {
      $this->assertText($term['literal'], 'The term was saved and appears on the node page.');
    }
  }
**/
}
