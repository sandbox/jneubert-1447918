<?php

class EconPersWebTaxonomy extends WebTaxonomy {

  /**
   * Implements WebTaxonomy::autocomplete().
   */
  public function autocomplete($string = '') {
    $term_info = econ_taxonomies_autocomplete($this->wt_name, $string);
    return $term_info;
  }

  /**
   * Implements WebTaxonomy::fetchTerm().
   */
  public function fetchTerm($term) {
    $term_info = econ_taxonomies_fetch_term_sparql($term);
    return $term_info;
  }
}
